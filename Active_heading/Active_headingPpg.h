#if !defined(AFX_ACTIVE_HEADINGPPG_H__B675342E_C602_43EC_929D_48FD90CE0969__INCLUDED_)
#define AFX_ACTIVE_HEADINGPPG_H__B675342E_C602_43EC_929D_48FD90CE0969__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Active_headingPpg.h : Declaration of the CActive_headingPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CActive_headingPropPage : See Active_headingPpg.cpp.cpp for implementation.

class CActive_headingPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CActive_headingPropPage)
	DECLARE_OLECREATE_EX(CActive_headingPropPage)

// Constructor
public:
	CActive_headingPropPage();

// Dialog Data
	//{{AFX_DATA(CActive_headingPropPage)
	enum { IDD = IDD_PROPPAGE_ACTIVE_HEADING };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CActive_headingPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIVE_HEADINGPPG_H__B675342E_C602_43EC_929D_48FD90CE0969__INCLUDED)
