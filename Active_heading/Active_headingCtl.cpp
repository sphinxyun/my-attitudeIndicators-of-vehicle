// Active_headingCtl.cpp : Implementation of the CActive_headingCtrl ActiveX Control class.

#include "stdafx.h"
#include "Active_heading.h"
#include "Active_headingCtl.h"
#include "Active_headingPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CActive_headingCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CActive_headingCtrl, COleControl)
	//{{AFX_MSG_MAP(CActive_headingCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CActive_headingCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CActive_headingCtrl)
	DISP_PROPERTY_NOTIFY(CActive_headingCtrl, "size", m_size, OnSizeChanged, VT_R8)
	DISP_FUNCTION(CActive_headingCtrl, "HeadingAngle", HeadingAngle, VT_EMPTY, VTS_R8)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CActive_headingCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CActive_headingCtrl, COleControl)
	//{{AFX_EVENT_MAP(CActive_headingCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CActive_headingCtrl, 1)
	PROPPAGEID(CActive_headingPropPage::guid)
END_PROPPAGEIDS(CActive_headingCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CActive_headingCtrl, "ACTIVEHEADING.ActiveheadingCtrl.1",
	0xcb28fc8e, 0xb66a, 0x4434, 0xaf, 0x5, 0xd7, 0xaf, 0x63, 0xda, 0xed, 0xbd)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CActive_headingCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DActive_heading =
		{ 0x56556915, 0x62be, 0x48be, { 0x88, 0x30, 0x5f, 0x77, 0x55, 0x27, 0x10, 0x8f } };
const IID BASED_CODE IID_DActive_headingEvents =
		{ 0x3fe4087f, 0x52fd, 0x4ce7, { 0x85, 0x5d, 0xb3, 0xd6, 0xb5, 0xbb, 0x1a, 0x93 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwActive_headingOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CActive_headingCtrl, IDS_ACTIVE_HEADING, _dwActive_headingOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::CActive_headingCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CActive_headingCtrl

BOOL CActive_headingCtrl::CActive_headingCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_ACTIVE_HEADING,
			IDB_ACTIVE_HEADING,
			afxRegInsertable | afxRegApartmentThreading,
			_dwActive_headingOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::CActive_headingCtrl - Constructor

CActive_headingCtrl::CActive_headingCtrl()
{
	InitializeIIDs(&IID_DActive_heading, &IID_DActive_headingEvents);

	// TODO: Initialize your control's instance data here.
	heading_angle=0;
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::~CActive_headingCtrl - Destructor

CActive_headingCtrl::~CActive_headingCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::OnDraw - Drawing function
//画箭头直线
void CActive_headingCtrl::DrawArrow(CDC* dc,CPoint p1,CPoint p2,double theta,int length)
{
    theta=3.1415926*theta/180;//转换为弧度
    double Px,Py,P1x,P1y,P2x,P2y;
    //以P2为原点得到向量P2P1（P）
    Px=p1.x-p2.x;

    Py=p1.y-p2.y;

    //向量P旋转theta角得到向量P1

    P1x=Px*cos(theta)-Py*sin(theta);

    P1y=Px*sin(theta)+Py*cos(theta);

    //向量P旋转-theta角得到向量P2

    P2x=Px*cos(-theta)-Py*sin(-theta);

    P2y=Px*sin(-theta)+Py*cos(-theta);

    //伸缩向量至制定长度

    double x1,x2;

    x1=sqrt(P1x*P1x+P1y*P1y);

    P1x=P1x*length/x1;

    P1y=P1y*length/x1;

    x2=sqrt(P2x*P2x+P2y*P2y);

    P2x=P2x*length/x2;

    P2y=P2y*length/x2;

    //平移变量到直线的末端

    P1x=P1x+p2.x;

    P1y=P1y+p2.y;

    P2x=P2x+p2.x;

    P2y=P2y+p2.y;

    //CClientDC dc(this);//获取客户窗口DC

    CPen pen,pen1,*oldpen;

    pen.CreatePen(PS_SOLID, 4, RGB(255,0, 0));

    pen1.CreatePen(PS_SOLID, 4, RGB(255,0, 0));

    oldpen=dc->SelectObject(&pen);

    dc->MoveTo(p1.x,p1.y);

    dc->LineTo(p2.x,p2.y);

    dc->SelectObject(&pen1);

    dc->MoveTo(p2.x,p2.y);

    dc->LineTo(P1x,P1y);

    dc->MoveTo(p2.x,p2.y);

    dc->LineTo(P2x,P2y);

    dc->SelectObject(oldpen);

}
void CActive_headingCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	//2018.3.22---绘制heading界面	
	double m_width=m_size;
	double m_height=m_size;
	CRect rect(0,0,m_height,m_width);
	//定义画笔
	CPen MyPen;
    //画刷  
    CBrush MyBrush; 	
	
	//画矩形框
	MyPen.CreatePen(PS_SOLID,4,RGB(119,136,153));
	MyBrush.CreateSolidBrush(RGB(0,139,139));
	pdc->SelectObject(MyPen);
	pdc->SelectObject(MyBrush);
	pdc->Rectangle(rect);
	
	//画圆盘
	CPen MyPen2;
    CBrush MyBrush2; 
	MyPen2.CreatePen(PS_SOLID,1,RGB(100,149,237));
    MyBrush2.CreateSolidBrush(RGB(100,149,237)); 
	pdc->SelectObject(MyPen2);
	pdc->SelectObject(MyBrush2);
	pdc->Ellipse(rect);	

	//画刻度 
	float len_kedu=25;                             //刻度长度
	int nofParts=12;                               //共标记12个刻度
	float zeta_Angle=(3.1415926/6);                //间隔弧度值
	float angle=0;                                 //初始角度为0
	float x,y,x1,y1,x2,y2;                         //刻度端点坐标
	float radis=m_height/2.0;                      //圆盘半径
	float font_width=15.0;                         //字体宽度
	CPen MyPen_kedu(PS_SOLID,2,RGB(138,43,226));   //刻度画笔
	int value_kedu=0;                              //刻度值，初值为0
	CString value;
	pdc->SelectObject(MyPen_kedu);
	pdc->SelectObject(MyBrush2);
	pdc->SetTextColor(RGB(138,43,226));
	//pdc->SetBkMode(TRANSPARENT);                 //透明  
	pdc->SetBkColor(RGB(100,149,237));
	for(int i=1;i<=nofParts;i++)
	{
		x=(float)(radis+(radis-len_kedu-font_width)*sin(angle));    
		y=(float)(radis-(radis-len_kedu-font_width)*cos(angle));
		x1=(float)(radis+(radis-font_width)*sin(angle));
		y1=(float)(radis-(radis-font_width)*cos(angle));
		x2=(float)(radis+radis*sin(angle));
		y2=(float)(radis-radis*cos(angle));
		
		//画出从(x,y)->(x1,y1)的直线
		pdc->MoveTo(x,y);     
		pdc->LineTo(x1,y1);
		
		//画数字
		CFont font;
		font.CreatePointFont(120,_T("Times New Roman"));	
		value.Format(_T("%d"),value_kedu);
		
		if(value_kedu==0)
			pdc->TextOut(x2-font_width/2+4,0,value);
		else if(value_kedu<90)
			pdc->TextOut(x1-5,y2,value);
		else if(value_kedu==90)
			pdc->TextOut(x1,y1-font_width/2,value);
		else if(value_kedu<180)
			pdc->TextOut(x1-7,y1-5,value);
		else if(value_kedu==180)
			pdc->TextOut(x1-font_width/2-5,y1,value);
		else if(value_kedu<270)
			pdc->TextOut(x2,y1-4,value);
		else if(value_kedu==270)
			pdc->TextOut(0,y2-font_width/2,value);
		else
			pdc->TextOut(x2,y2,value);
		angle+=zeta_Angle;
		value_kedu+=30;
	}
	
	//画圆点
	CPen MyPen3;
    CBrush MyBrush3; 
	MyPen3.CreatePen(PS_SOLID,1,RGB(255,0,0));
    MyBrush3.CreateSolidBrush(RGB(255,0,0)); 
	pdc->SelectObject(MyPen3);
	pdc->SelectObject(MyBrush3);
	pdc->Ellipse(radis-8,radis-8,radis+8,radis+8);	
	
	//画当前角度值
	CString CAngle;
	double hangle=heading_angle*180/3.1415926;
	CAngle.Format(_T("%.3f"),hangle);
	CPen Pen_Angle(PS_SOLID,2,RGB(255,0,0));
	pdc->SelectObject(Pen_Angle);
	pdc->SelectObject(MyBrush2);
	pdc->TextOut(radis-20,radis+10,CAngle);

	//画字符“heading”
	pdc->TextOut(radis-25,radis+25,_T("Heading"));

	//画指针
	int len_Arrow=radis-font_width-len_kedu;
	CPoint p1(radis,radis),p2;
	p2.x=radis+len_Arrow*sin(heading_angle);
	p2.y=radis-len_Arrow*cos(heading_angle);
	DrawArrow(pdc,p1,p2,20,20);

	//清除 
	MyPen.DeleteObject();
	MyPen2.DeleteObject();
	MyPen3.DeleteObject();
    MyBrush.DeleteObject();  
    MyBrush2.DeleteObject();  
    MyBrush3.DeleteObject();  
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::DoPropExchange - Persistence support

void CActive_headingCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
	PX_Double(pPX,"size",m_size,200);
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::OnResetState - Reset control to default state

void CActive_headingCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl::AboutBox - Display an "About" box to the user

void CActive_headingCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_ACTIVE_HEADING);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl message handlers

void CActive_headingCtrl::HeadingAngle(double angle) 
{
	// TODO: Add your dispatch handler code here
	heading_angle=angle*3.1415926/180;
}

void CActive_headingCtrl::OnSizeChanged() 
{
	// TODO: Add notification handler code
	InvalidateControl();
	SetModifiedFlag();
}
