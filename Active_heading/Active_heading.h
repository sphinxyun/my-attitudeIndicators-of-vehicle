#if !defined(AFX_ACTIVE_HEADING_H__EAC897F2_4A6E_45BE_A5D1_DF8AA0D156CE__INCLUDED_)
#define AFX_ACTIVE_HEADING_H__EAC897F2_4A6E_45BE_A5D1_DF8AA0D156CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Active_heading.h : main header file for ACTIVE_HEADING.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CActive_headingApp : See Active_heading.cpp for implementation.

class CActive_headingApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIVE_HEADING_H__EAC897F2_4A6E_45BE_A5D1_DF8AA0D156CE__INCLUDED)
