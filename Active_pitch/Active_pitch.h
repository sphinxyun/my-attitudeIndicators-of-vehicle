#if !defined(AFX_ACTIVE_PITCH_H__6970B546_1DCD_4DE3_97EE_993CAE32D303__INCLUDED_)
#define AFX_ACTIVE_PITCH_H__6970B546_1DCD_4DE3_97EE_993CAE32D303__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Active_pitch.h : main header file for ACTIVE_PITCH.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CActive_pitchApp : See Active_pitch.cpp for implementation.

class CActive_pitchApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIVE_PITCH_H__6970B546_1DCD_4DE3_97EE_993CAE32D303__INCLUDED)
